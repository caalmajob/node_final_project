const Travel = require('../models/Travel');

const indexGet = async (req, res, next) => {
    try {
        const travel = await Travel.find();
        const journey = travel.slice(-4);
        return res.render('index', { journey, user: req.user});
    } catch (error) {
        next(new Error(error));
    }
}

module.exports = {
    indexGet,
}