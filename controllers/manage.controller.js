const Travel = require('../models/Travel');
const User = require('../models/User');

const indexGet = async (req, res, next) => {
    try {
        let travels;
        const id = req.user._id;
        if (req.user.role === "admin") {
            travels = await Travel.find({ contactId: id });
        } else {
            const user = await User.findById(id).populate('likes');
            travels = user.likes;
        }
        console.log(travels);
        return res.render('profile', { travels, user: req.user, isAdmin: req.user.role === "admin" });
    } catch (error) {
        next(new Error(error));
    }
}

const editIdPut = async (req, res, next) => {
    try {
        let image;
        if (req.file) {
            image = req.file.filename;
        };
        const { id } = req.params;
        const { title, description, price, country } = req.body;
        const editTravel = await Travel.findByIdAndUpdate(id, {
            title, description, country, price, image
        }, {
            new: true
        });
        return res.redirect('/manage/');
    } catch (error) {
        next(error);
    }
}

const modifyGet = async (req, res, next) => {
    try {
        const { id } = req.params;
        const travelDetails = await Travel.findById(id);
        console.log('aqui', travelDetails)
        return res.render('modify', { travelDetails, user: req.user });
    } catch (error) {
        console.log(error);
        next(error);
    }
}

const deleteDel = async (req, res, next) => {
    try {
        const { id } = req.params;
        const deleted = await Travel.findByIdAndDelete(id);
        if (deleted) {
            return res.status(200).redirect("/manage")
        }
        return res.status(200).redirect("/manage");
    } catch (error) {
        next(error);
    }
}

const cartGet = async (req, res, next) => {
    try {
        let cart;
        const id = req.user._id;

        const user = await User.findById(id).populate('travels');
        cart = user.travels;
        return res.render('cart', { cart, user: req.user});
    } catch (error) {
        next(new Error(error));
    }
}

const cartPut = async (req, res, next) => {
    try {
        const { id } = req.params;
        const { travelId } = req.body;

        const editUser = await User.findByIdAndUpdate(id, {
            $addToSet: { travels: travelId },
        }, {
            new: true
        });
        return res.redirect('/manage/');
    } catch (error) {
        next(error);
    }
}

module.exports = {
    indexGet,
    editIdPut,
    modifyGet,
    deleteDel,
    cartGet,
    cartPut,
}