const passport = require('passport');

//--------Register-------------//

const registerGet = (req, res, next) => {
    return res.render('register', { user: req.user });
};

const registerPost = (req, res, next) => {
    const { email, password } = req.body;

    if (!email || !password) {
        const error = new Error('User and password are required');
        return res.render('register', { error: error.message });
    }

    passport.authenticate('registro', (error, user) => {

        if (error) {
            return res.render('register', { error: error.message });
        }

        return res.redirect('/auth/login');
    })(req);
}

//------------Login-----------------//

const loginGet = (req, res, next) => {
    return res.render('login', { user: req.user });
}

const loginPost = (req, res, next) => {
    const { email, password } = req.body;

    if (!email || !password) {
        const error = new Error('User and password are required')
        return res.render('login', { error: error.message });
    }

    passport.authenticate('acceso', (error, user) => {
        if (error) {
            return res.render('login', { error: error.message });
        }

        req.logIn(user, (error) => {
            if (error) {
                return res.render('login', { error: error.message });
            }

            return res.redirect('/');
        })
    })(req, res, next)
}

//------------------Logout------------------------//

const logoutPost = (req, res, next) => {
    if (req.user) {
        req.logOut();

        req.session.destroy(() => {
            res.clearCookie('connect.sid');
            return res.render('index', { message: 'Usuario deslogeado con éxito' });
        })
    } else {
        return res.redirect('/', { message: 'User not found' });
    }
}


module.exports = {
    registerGet,
    registerPost,
    loginGet,
    loginPost,
    logoutPost,
}