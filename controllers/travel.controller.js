const Travel = require('../models/Travel');
const User = require('../models/User');


const indexGet = async (req, res, next) => {
    try {
        const travel = await Travel.find();
        return res.render('travels', { travel, user: req.user});
    } catch (error) {
        next(new Error(error));
    }
}

const createTravelGet = (req, res) => {
    return res.render('create', { user: req.user });
}

const createTravelPost = async (req, res, next) => {
    try {
        const { title, description, country, price, contactId } = req.body;
        console.log('CLOUDINARY', req.file_url)
        const image = req.file_url;

        const newTravel = await new Travel({
            title, description,
            country, price, contactId, image
        });

        await newTravel.save();

        return res.redirect('/destination/travels');

    } catch (error) {
        next(error);
    }
}

const searchTravelGet = async (req, res, next) => {
    try {
        const { title } = req.query;
        if (!title) {
            const error = new Error('No se encuentra ninguna coincidencia')
            return res.render('travels', { user: req.user, error: error.message });
        }
        const travel = await Travel.find({ title });
        return res.render('travels', { travel, user: req.user});
    } catch (error) {
        next(new Error(error));
    }
}

const travelByIdGet = async (req, res, next) => {
    try {
        const { id } = req.params;
        const travelDetails = await Travel.findById(id);
        return res.render('travel', { travelDetails, user: req.user });
    } catch (error) {
        console.log(error);
        next(error);
    }
}

const travelLikePut = async (req, res, next) => {
    try {
        const { id } = req.params;
        const { travelId } = req.body;

        const editUser = await User.findByIdAndUpdate(id, {
            $addToSet: { likes: travelId},
        }, {
            new: true
        });
        return res.redirect('/manage/');
    } catch (error) {
        next(error);
    }
}

module.exports = {
    indexGet,
    createTravelGet,
    createTravelPost,
    searchTravelGet,
    travelByIdGet,
    travelLikePut,
}