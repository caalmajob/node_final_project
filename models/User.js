const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    email: { type: String, required: true },
    password: { type: String, required: true },
    likes: [{ type: mongoose.Types.ObjectId, ref: 'Travel' }],
    travels: [{ type: mongoose.Types.ObjectId, ref: 'Travel' }],
    role: { type: String, enum: ['user', 'admin'], default: 'user'},
}, {
    timestamps: true,
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
