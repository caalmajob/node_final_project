const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TravelSchema = new Schema({
    title: { type: String, required: true },
    description: { type: String, required: true },
    country: { type: String, required: true },
    price: { type: Number, required: true },
    image: {type: String },
    contactId: { type: mongoose.Types.ObjectId, ref: 'User'},
}, {
    timestamps: true,
});

const Travel = mongoose.model('Travel', TravelSchema);

module.exports = Travel;
