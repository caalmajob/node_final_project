require('dotenv').config();
const db = require('../db');
const mongoose = require('mongoose');
const Travel = require('../models/Travel');

const travel = [
    {
        title: "El paraíso se llama Riviera Maya",
        description: "¿Quieres pasar unas vacaciones ideales al mejor precio? Pues no lo dudes, disfruta de esta oferta que incluye los vuelos desde Madrid, traslados y estancia en el Hotel Grand Palladium Kantenah/Colonial 5* en TI.",
        country: "México",
        price: 1770,
        imgage: 'https://www.viajeselcorteingles.es/imagen/dest/america/mexico/riviera_maya/img_dest_mexico_riviera_maya_301x184_01.jpg',
        contactId: "6064de4c3bca361917a9ec8e",
},
{
    title: "París",
    description: "¿Hay algo más apetecible que una escapada a París? Aprovecha el fin de semana y déjate seducir por la ciudad más romántica de Europa.",
    country: "Francia",
    price: 185,
    image: 'https://www.viajeselcorteingles.es/imagen/pt/mic/paris/paris_en_48h/img_mic_paris_en_48h_arco_del_triunfo_301x184.jpg',
    contactId: "6064de4c3bca361917a9ec8e",
}
];

console.log( 'db', db);
mongoose
    .connect(db.DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async () => {
    console.log('Deleting all travels...');
    const allTravels = await Travel.find();
    

    if(allTravels.length) {
        await Travel.collection.drop();
    }
    })
    .catch(error => {
        console.log('Error deleting data: ', error)
    })
    .then( async() => {
        await Travel.insertMany(travel);
        console.log('Successfully added travels to DB...');
    })
    .finally(() => mongoose.disconnect());
