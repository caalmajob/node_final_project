require('dotenv').config();
const mongoose = require('mongoose');
const db = require('../db');
const User = require('../models/User');

const users = [
    {
        email: "mara@gmail.com",
        password: "12345",
        role: 'user'
    },
    {
        email: "carlos@gmail.com",
        password: "12345",
        role: 'admin'
    },
];

mongoose
    .connect(db.DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async () => {
        console.log('Deleting all users...');
        const allUsers = await User.find();


        if (allUsers.length) {
            await User.collection.drop();
        }
    })
    .catch(error => {
        console.log('Error deleting data: ', error)
    })
    .then(async () => {
        await User.insertMany(users);
        console.log('Successfully added users to DB...');
    })
    .finally(() => mongoose.disconnect());
