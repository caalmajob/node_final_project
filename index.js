require('dotenv').config();
const express = require('express');
const passport = require('passport');
const path = require('path');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const db = require('./db');
const indexRoutes = require('./routes/index.routes');
const authRoutes = require('./routes/auth.routes');
const travelsRoutes = require('./routes/travel.routes');
const manageRoutes = require('./routes/manage.routes');
const methodOverride = require('method-override');




require('./passport/passport');
db.connect();

const PORT = process.env.PORT || 3000;

const app = express();

app.use(session({
  // secret: 'AasQsfi.123-@',
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: false,
  cookie: {
      maxAge: 48 * 60 * 60 * 1000
  },
  store: MongoStore.create({ mongoUrl: db.DB_URL}),
}));

app.set('viewns', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(express.static(path.join(__dirname, 'public')));

app.use(passport.initialize());
app.use(passport.session());

app.use(methodOverride('_method'));

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.use('/', indexRoutes);
app.use('/auth', authRoutes);
app.use('/manage', manageRoutes);
app.use('/destination', travelsRoutes);


app.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
})

app.use((error, req, res, next) => {
  console.log(error);
    return res.status(error.status || 500).render('error', 
    {error: error.message || 'Unexpected error'});
});
// -------------SERVER CONFIG -------------------------------------
const serverCallback = () => {
  console.log(`Servidor escuchando en http://localhost:${PORT}`);
};

app.listen(PORT, serverCallback);
