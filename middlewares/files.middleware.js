const multer = require('multer');
const path = require('path');
const fs = require('fs');
const cloudinary = require('cloudinary').v2;

console.log('CLOUDINARY', process.env.CLOUDINARY_URL)


const VALID_TYPES_FILES = ['image/png', 'image/jpg', 'image/jpeg']

const storage = multer.diskStorage({
    filename: (req, file, cb) => {
        cb(null, `${Date.now()}-${file.originalname}`)
    },
    destination: (req, file, cb) => {
        cb(null, path.join(__dirname, '../public/uploads'));
    }
})

const fileFilter = (req, file, cb) => {
    if (VALID_TYPES_FILES.includes(file.mimetype)) {
        cb(null, true)
    } else {
        const error = new Error('¡Invalid file type!');
        cb(error);
    }
}


const upload = multer({
    storage,
    fileFilter,
});

const uploadToCloudinary = async (req, res, next) => {
    console.log('imagen recibida', req.file)
    if (req.file) {
        try {
            const filePath = req.file.path;
            const image = await cloudinary.uploader.upload(filePath);
            await fs.unlinkSync(filePath);

            req.file_url = image.secure_url || 'https://blog.educacionit.com/wp-content/uploads/2018/09/shutterstock-10338536170938-620x354-01-750x410.jpg';
            return next();
        } catch (error) {
            return next(error);
        }
    } else {
        return next();
    }
}

module.exports = { upload, uploadToCloudinary };
