const express = require('express');
const travelController = require('../controllers/travel.controller');
const Travel = require('../models/Travel');
const User = require('../models/User');
const { upload, uploadToCloudinary } = require('../middlewares/files.middleware');
const { isAdmin, isAuthenticated } = require ('../middlewares/auth.middleware');
const router = express.Router();

router.get('/travels', travelController.indexGet);

router.get('/create', [isAdmin], travelController.createTravelGet);

router.post('/create', [isAdmin ,upload.single('avatar'), uploadToCloudinary ], travelController.createTravelPost);

router.get('/search', travelController.searchTravelGet);

router.get('/travel/:id', travelController.travelByIdGet);

router.put('/like/:id', isAuthenticated, travelController.travelLikePut)


module.exports = router;
