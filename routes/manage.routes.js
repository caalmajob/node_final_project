const express = require('express');
const manageController = require('../controllers/manage.controller');
const User = require('../models/User');
const { upload } = require('../middlewares/files.middleware');
const { isAuthenticated, isAdmin } = require('../middlewares/auth.middleware');
const router = express.Router();

router.get('/', [isAuthenticated], manageController.indexGet);

router.put('/edit/:id', [isAdmin, upload.single('avatar')], manageController.editIdPut);

router.get('/modify/:id', [isAdmin], manageController.modifyGet);

router.delete('/delete/:id', [isAdmin], manageController.deleteDel);

router.get('/cart', [isAuthenticated], manageController.cartGet);

router.put('/cart/:id', [isAuthenticated],  manageController.cartPut);


module.exports = router;
